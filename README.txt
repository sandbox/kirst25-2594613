######
###### --- Mobile Push Menu - mp-menu module README.txt --- 
######

#### --- The initial settings are found in the module configuration: --- ####

  User chooses a menu and screen size they want it to appear.

#### --- Styles eg. Colours etc. Should be written & overwritten in themes stylesheet, not module. --- ####

  // EXAMPLE //
  #mp-button{
    background: #3d68b9;
  }
  #mp-button:hover{
    background: #5b82ca;
    color: #fff;
  }
  body.mp-menu ul.mp-first-level,
  body.mp-menu ul.mp-second-level,
  body.mp-menu ul.mp-third-level {
    background: #222;
  }