<?php

/**
 * Implementation of hook_menu()
 * 
 */
function mp_menu_menu() {
  $items = array();

  $items['admin/config/user-interface/mp-menu'] = array(
    'title' => 'Mobile Push Menu',
    'description' => 'Mobile push menu settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mp_menu_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implementation of hook_form()
 * 
 */
function mp_menu_form($form, &$form_state) {
  $current_theme = variable_get('theme_default','none');
  $themes = list_themes();
  $theme_object = $themes[$current_theme];

  $system_menus = menu_get_menus();

  $form['description'] = array( 
    '#markup' => '<p>This adds your chosen menu to $page_top region</p>' 
  );
  /*--Set the menu--*/
  $form['mp_menu_id'] = array(
    '#type' => 'radios',
    '#options' => $system_menus,
    '#title' => t('Menus'),
    '#default_value' => variable_get('mp_menu_id', 'main-menu'),
    '#required' => TRUE,
    '#description' => t('Choose which menu tree to use.'),
  );
  /*--Set the menu level--*/ 
  $form['mp_menu_level'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of levels'),
    '#default_value' => variable_get('mp_menu_level', '3'),
    '#size' => 7,
    '#maxlength' => 7,
    '#description' => t('Set the number levels of the menu. (Max 3)'),
    '#required' => FALSE,
    '#field_suffix' => 'Levels',
  );
  /*--Set the menu breakpoint--*/ 
  $form['mp_menu_breakpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakpoint'),
    '#default_value' => variable_get('mp_menu_breakpoint', 'all and (min-width:480px)'),
    '#size' => 21,
    '#maxlength' => 155,
    '#description' => t('Set when you would like the mobile menu to kick in.'),
    '#required' => TRUE,
    '#field_prefix' => '@media',
  );
  /*--Disable push effect--*/
  $form['mp_menu_pushable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable push effect of the page'),
    '#default_value' => variable_get('mp_menu_pushable', 1),
    '#required' => FALSE,
  );
  // When using bootstrap
  if(isset($theme_object->info['base theme'])) {
    if($theme_object->info['base theme'] == 'bootstrap') {
      $form['mp_menu_pushable']['#suffix'] = 'In Bootstrap a page wrapper with an id of "page" should be added in page.tpl.php for the push effect. E.g. \'&lt;div id="page"&gt;[page]&lt;/div&gt;\'';
      $form['mp_menu_pushable']['#disabled'] = TRUE;
    }
  }
  /*--Enable overlay effect of the page--*/
  $form['mp_overlay'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable overlay effect of the page'),
    '#default_value' => variable_get('mp_overlay', 0),
    '#required' => FALSE,
  );
  /*--Hide Menu title--*/
  $form['mp_menu_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Menu title'),
    '#default_value' => variable_get('mp_menu_header', 0),
    '#required' => FALSE,
  );
  /*--Hide another menu option--*/ 
  $form['mp_menu_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Hide a menu'),
    '#default_value' => variable_get('mp_menu_name'),
    '#size' => 20,
    '#maxlength' => 255,
    '#description' => t('This will hide your menu/element when the push menu is active. Use "#your-element-id" or ".your-element-class"'),
    '#required' => FALSE,
  );
  /*--Set the button--*/
  $form['mp_menu_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable menu button'),
    '#default_value' => variable_get('mp_menu_button', 1),
    '#description' => t('Disable to add the menu button manually to your site'),
    '#required' => FALSE,
  );

  $form['mp_menu_button_text'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="code">Button html:<br/><div style="background: #eee;padding: 10px;font-family: monospace, serif;">&lt;div id="mp-button"&gt;&lt;span class="mp-icon-bars"&gt;&lt;/span&gt;&lt;/div&gt;</div></div>',
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_permission().
 * 
 */
function mp_menu_permission() {
  return array(
    'Administor the mp menu' => array(
      'title' => t('Administer mp menu'),
      'description' => t('Change settings for the mp menu'),
    ),
  );
}

/**
 * Add module class to body.
 * 
 */
function mp_menu_preprocess_html(&$vars){
  $push = 1;
  $push = variable_get('mp_menu_pushable');
  if($push == 1){
    $vars['classes_array'][] = "mp-menu-push";
  }
  
  $overlay = variable_get('mp_overlay');
  if($overlay == TRUE) {
    $vars['classes_array'][] = "mp-overlay";
  }
}

/**
 * Alter metatag viewport for scale.
 * 
 */
function mp_menu_html_head_alter(&$head_elements) {
  if (!path_is_admin(current_path()) && !empty($head_elements['mobile_viewport'])) {
    $head_elements['mobile_viewport']['#attributes']['content'] = 'width= device-width, initial-scale= 1';
  } 
}

/**
 * Implementation of hook_preprocess_page().
 * 
 */
function mp_menu_preprocess_page(&$vars){
  if (!path_is_admin(current_path())) {
    drupal_add_js(drupal_get_path('module', 'mp_menu'). '/js/mp_index.js');
  }
}

/**
 * Implementation of hook_init().
 * 
 */
function mp_menu_init() {
  $button = variable_get('mp_menu_button', 1);
  $hide_menu = variable_get('mp_menu_name');
  $media = variable_get('mp_menu_breakpoint');
  
  drupal_add_css(drupal_get_path('module', 'mp_menu') . '/css/mp_media.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE, 'media' => $media));
  
  if(!empty($hide_menu)) {
    drupal_add_css($hide_menu.'{ display: none; }', array('type' => 'inline', 'group' => CSS_DEFAULT, 'every_page' => TRUE, 'media' => $media));
  }
  
  if(!empty($button)) {
    drupal_add_css(drupal_get_path('module', 'mp_menu') . '/css/mp_button.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE,));
  }
}

/**
 * Adds mp_menu to page top region.
 * 
 */
function mp_menu_page_build(&$page) {
  if (!path_is_admin(current_path())) {
    $page['page_top'] = array(
      '#weight' => -50,
      '#markup' => _mp_menu_mp_menu_content(),
    );
  }
}

/**
 * Block content.
 * 
 */
function _mp_menu_mp_menu_content() {
  $menus_id = variable_get('mp_menu_id');
  $id = $menus_id;
  $menus = menu_get_menus();

  $level = variable_get('mp_menu_level');

  if ($level < 3) {
    $level = variable_get('mp_menu_level');
  } else {
    $level = 3;
  }

  $menu = menu_tree_all_data($id, NULL, $level);
  
  $button = variable_get('mp_menu_button', 1);
  $buttonhtml = '';
  
  if(!empty($button)) {
    $buttonhtml = '<div id="mp-button" ><span class="mp-icon-bars"></span></div>';
  }
  
  if(!empty($menu)){ 
    $output = $buttonhtml;
    $output .= '<div id="mp-menu">';
    $output .= mp_menu_theme_dropdown_menu($menu, $id);
    $output .= '</div>';
    return $output;
  }
}

/**
 * Builds cycles through the provided menu tree.
 *
 */
function mp_menu_theme_dropdown_menu($menu_tree, $menu_delta, $menu_inline = FALSE) {
  // Remove hidden links.
  mp_menu_remove_hidden($menu_tree);

  // Root <ul>.
  $menu_classes = array(
    $menu_delta,
    'mp-menu',
    'mp-first-level'
  );

  if ($menu_inline) {
    $menu_classes[] = 'inline';
  }

  $menu_tree_list = mp_menu_build_menu($menu_tree);

  $menu_tree_rendered = theme('item_list__mp_menu_menu', array(
    'items' => $menu_tree_list,
    'attributes' => array(),
    'list_attributes' => array('class' => $menu_classes),
  ));
  return $menu_tree_rendered;
}

/**
 * Alters menu tree data.
 *
 */
function mp_menu_build_menu($menu_tree_data, &$parent = null){

  $menu_tree_list = array();

  // Recurse trough the menu data tree
  foreach($menu_tree_data as $menu_item_key => $link) {
    $attributes = isset($link['link']['options']) ? $link['link']['options'] : array();

    $classes = array();

    // Sub menu classes initiate
    $sub_menu_classes = array();

    if ($link['link']['href'] == $_GET['q']){
      $classes[] = 'active-trail';
      $classes[] = 'active';
      $parent['class'][] = 'active-trail';
    }

    if ($link['below']) {
      $classes[] = 'mp-expanded';
    }

    if (!$link['link']['p2']) {
      $sub_menu_classes[] = 'mp-second-level';
    }
    if ($link['link']['p2'] && !$link['link']['p3']) {
      $sub_menu_classes[] = 'mp-third-level';
    }

    // Add the item to the list.
    $menu_tree_list[$menu_item_key] = array(
      'data' => l($link['link']['title'], $link['link']['href'], $attributes),
      'class' => $classes,
      'list_attributes' => array(
        'class' => $sub_menu_classes,
      ),
    );

    if ($link['below']) {
      $menu_tree_list[$menu_item_key]['children'] = mp_menu_build_menu($link['below'],$menu_tree_list[$menu_item_key]);
    }
  }

  return $menu_tree_list;
}

/**
 * Alters a menu tree array to remove any disabled menu items
 *
 */
function mp_menu_remove_hidden(&$menu_tree_data) {
  foreach ($menu_tree_data as $menu_item_key => &$menu_item) {
    // If the top level link is disabled, remove it before digging deeper.
    if ($menu_item['link']['hidden']) {
      unset($menu_tree_data[$menu_item_key]);
      continue;
    }
    // Run disabled menu item check for all child items
    if ($menu_item['below']) {
      mp_menu_remove_hidden($menu_item['below']);
    }
  }
}

/**
 * Implementation of hook_theme().
 *
 */
function mp_menu_theme($existing, $type, $theme, $path) {
  return array(
    'item_list__mp_menu_menu' => array(
      'variables' => array(),
      'function' => 'mp_menu_theme_menu_item_list',
    ),
  );
}

/**
 * Implementation of theme_item_list
 * 
 * List Output
 * 
 */
function mp_menu_theme_menu_item_list($variables) {
  $items = $variables['items'];
  $attributes = $variables['attributes'];
  $list_attributes = $variables['list_attributes'];
  $output = '';

  if (!empty($items)) {
    $output .= '<ul' . drupal_attributes($list_attributes) . '>';
    $num_items = count($items);
    $i = 0;

    if($attributes) {
      $output .= '<li class="mp-back"><span class="mp-icon-angle-left"></span>Back</li>';
    } else {
      if (variable_get('mp_menu_header') == 0 ) {
        $output .= '<li class="mp-title">Menu</li>';
      }
    }

    foreach ($items as $item) {
      $attributes = array();
      $list_attributes = array();
      $children = array();
      $data = '';
      $i++;
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          elseif ($key == 'list_attributes') {
            $list_attributes = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= mp_menu_theme_menu_item_list(array(
          'items' => $children,
          'title' => NULL,
          'attributes' => $attributes,
          'list_attributes' => $list_attributes
        ));
      }
      if ($i == 1) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items) {
        $attributes['class'][] = 'last';
      }

      if(count($children) > 0) {
        $output .= '<li' . drupal_attributes($attributes) . '><span class="arrow mp-icon-angle-right"></span>' . $data . "</li>\n";
      } 
      else {
        $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
      }
    }
    $output .= '</ul>';
  }
  return $output;
}
