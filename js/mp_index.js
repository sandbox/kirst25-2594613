/*
 *  Mobile Push Menu - mp-menu module JS
 */

(function($) {

  Drupal.behaviors.mobilePushMenu = {
    attach: function (context, settings) {

      $('body').once('mp-menu', function() {

        // **           Menu Actions         ** //

        // Variables  
        $list = $('#mp-menu .mp-first-level');
        $secondMenu = $('#mp-menu .mp-second-level');
        $thirdMenu = $('#mp-menu .mp-third-level');
        $button = $('#mp-button');

        // Click Events
        $button.click(function(){
          $(this).toggleClass('active').children('span').toggleClass('mp-icon-bars mp-icon-close');
          $('body').toggleClass('mp-pushed');
          $('html').toggleClass('mp-pushed');
          $('li.mp-expanded').removeClass('pushed');
        }); 

        $('> li.mp-expanded span', $list).click(function(){
          $(this).parent().addClass('pushed');
        });  

        $('> .mp-back', $secondMenu).click(function(){
          $('> li.mp-expanded', $list).removeClass('pushed');
        });

        $('> .mp-back', $thirdMenu).click(function(){
          $('> li.mp-expanded', $secondMenu).removeClass('pushed');
        });

        var vernums = $.fn.jquery.split('.');
        if (parseInt(vernums[0]) > 0 && parseInt(vernums[1]) >= 7) {
          $('body').on('click touchstart', function(event) {
            if($('body').hasClass('mp-pushed')){ 
              event.preventDefault();
            }
            $button.removeClass('active').children('span').removeClass('mp-icon-close').addClass('mp-icon-bars');
            $('body, html, #page').removeClass('mp-pushed');
            $('li.mp-expanded').removeClass('pushed');
          });

          $('#mp-menu, #mp-button').on('click touchstart', function(event) {
            event.stopPropagation();
          });
        }

        // **             End            ** //

      });
    }
  };
})(jQuery);